# CLI Commands for security analysis

A listing of all the commands I presented in a tutorial on this topic on my site. The article on my site is much better organized.


### Get users listed in the passwd file
Grab the first colummn in the /etc/passwd file and print its contents.    
`# awk -F: '{print $1}' /etc/passwd`  


### Print environment variables
`# printenv`  


### Find a directory by name
`# find / -type d -name <directory> 2>/dev/null`  


### Get the gecos field for a user in the passwd file
`# grep guest /etc/passwd | cut -d ":" -f 5`  


### Run a command as another user
This will run the `strings` command as a user named guest against the example.blob file in /home/bin.  
`# sudo -u guest strings /home/bin/example.blob`  


### Locate a string in a file when the directory is known
Find the string "home", case insentive in any file in the /var/www directory.  
`# grep -i -R home /var/www`  

To filter down to searching a particular type of file.  
`# grep -i -R home /var/www/*.html`


### lsof  
`lsof` is used to discover which processes opened which files. Running the straight `lsof` command isn't a great idea. Since everything in UNIX/Linux is a file, you are going 
to get a wall of information presented to you. It is important to understand the column heads, however.

| COMMAND | PID        | USER     | FD             | TYPE         | DEVICE                               | SIZE/OFF               | NODE NAME |
| ------  | ----       |-----     |----            |------        |--------                              |----------              |-----------|
| Process | Process ID | Username |File Descriptor | Type of File | Device Numbers (refer to man 8 lsof) | Size of File or Offset | Node number of the local file |

To display files opened by a particular user.  
`lsof -u guest`  

To display processes running on specific port, for processes running on port 22. -i restricts processes to those with IP information.  
`lsof -i TCP:22` you can also supply a range of ports `lsof -i TCP:1-1024`  

You can look at processes start by a particular user.  
`lsof -i -u guest` you can also exclude users (look at all but) `lsof -i -u^guest`  

### Decoding
#### Base64
The way I encode base64 is  
`echo 'stringtoencode' | openssl base64`

To decode
`echo 'base64encodedstring' | openssl base64 -d`  


#### Hex
For hexadecimal the way I encode is similar to that of base64  
`echo 'stringtoencode' | xxd -p`  
This will output `737472696e67746f656e636f64650a`.  

If for some particular reason you need all caps you can use the -u flag.  
`echo 'stringtoencode' | xxd -p -u`  
This will output `737472696E67746F656E636F64650A`  

To decode hex
`echo '737472696E67746F656E636F64650A' | xxd -r -p`  




